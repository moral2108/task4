variable "ami_id" {
  default = "ami-09e67e426f25ce0d7"
}
variable "bastion_subnet_id" {
  default = "subnet-076ce28ec9d018a8e"
}
variable "private_subnet_id" {
  default = "subnet-01b38d837d91a6abf"
}
variable "type_inst" {
  default = "t2.micro"
}
variable "inst_tag" {
  default = "bastion_inst"
}
variable "my_ip" {
  default = "103.211.53.43/32"
}
variable "id_vpc" {
  default = "vpc-0b4e7f3df98628f23"
}
variable "priv_inst_tag" {
  default = "private_inst"
}
