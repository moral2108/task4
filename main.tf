#bastion instance
provider "aws" {
  region = "us-east-1"
}
resource "aws_security_group" "main" {
  vpc_id      = var.id_vpc
  ingress {
      description      = "My security Group"
      from_port        = 22
      to_port          = 22
      protocol         = "tcp"
      cidr_blocks      = ["103.211.53.43/32"]
    }
  egress {
      from_port = 0
      to_port = 0
      protocol = "-1"
      cidr_blocks = ["0.0.0.0/0"]
  }
  tags = {
    Name = "bastion_sg"
  }
}
resource "aws_instance" "main" {
  ami           = var.ami_id
  instance_type = var.type_inst
  vpc_security_group_ids = [aws_security_group.main.id]
  subnet_id = var.bastion_subnet_id
  associate_public_ip_address = true
  key_name = "aws_sample_keypair4"
  tags = {
    Name = var.inst_tag
  }
}



#private instance

resource "aws_instance" "private" {
  ami           = var.ami_id
  instance_type = var.type_inst
  vpc_security_group_ids = [aws_security_group.private.id]
  subnet_id = var.private_subnet_id
  associate_public_ip_address = false
  key_name = "aws_sample_keypair4"
  tags = {
    Name = var.priv_inst_tag
  }
}

resource "aws_security_group" "private" {
  vpc_id      = var.id_vpc
  ingress {
      description      = "My private security Group"
      from_port        = 22
      to_port          = 22
      protocol         = "tcp"
      cidr_blocks      = [join("/", [aws_instance.main.public_ip,"32"])]
    }
  egress {
      from_port = 0
      to_port = 0
      protocol = "-1"
      cidr_blocks = ["0.0.0.0/0"]
  }
  tags = {
    Name = "my_private_sg"
  }
}